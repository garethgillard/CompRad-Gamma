# CompRad-Gamma

This project is a comparative radiation experiment of the sensitivty of two plant species, _Arabidopsis thaliana_ and Norway spruce (_Picea abies_) to increasing dosages of gamma radiation. This repo contains the code, data, and results, for the RNA-seq and differential gene expression analysis of the project. See associated publication Bhattacharjee _et al_ 2023.

## Analysis

The same data analysis of RNA-seq counts was conducted on the two species under their respective directories:
- `arabidopsis`
- `spruce`

There are similar R markdown scripts under each species directory to take in `data` and generate the `results` and `figures`.

The results of each analysis can be downloaded and viewed in the html reports from these markdowns:
- [arabidopsis_analysis.html](https://gitlab.com/garethgillard/CompRad-Gamma/-/raw/master/arabidopsis/arabidopsis_analysis.html?inline=false)
- [spruce_analysis.html](https://gitlab.com/garethgillard/CompRad-Gamma/-/raw/master/spruce/spruce_analysis.html?inline=false)

The contents of the subfolders are described below.

## Data

### Gene expression
Gene level count and tpm (transcripts per million) values for experiment samples. Different outputs from several counting programs (salmon, kallisto, htseq) were obtained, but used in this analysis were salmon counts and tpm outputs. R scritps `collect_count_data.R` creates these tables from a BCBio pipeline output. Sample information, count data, protocols, and raw RNA-seq are available for spruce and Arabidopsis in ArrayExpress under acessions [E-MTAB-8081](https://www.ebi.ac.uk/biostudies/arrayexpress/studies/E-MTAB-8081) and [E-MTAB-11120](https://www.ebi.ac.uk/biostudies/arrayexpress/studies/E-MTAB-11120), respectively.

### Gene annotations
Gene Ontology (GO) annotation tables for spruce and Arabidopsis were downloaded from plantgenie.org and arabidopsis.org databases, respectively.

## Figures
Selection of figures outputed from the markdown reports used in the manuscript main and supplementary figures.

## Results
Saved results from the markdown reports. These include:
- DESeq results of differential expression analysis, per gamma dosage level: `deseq.RData`
- Annotated table of differentially expressed genes (FDR<0.05), with gene names, descriptions, GO annotations: `annotated_DEGs.tsv`
- GO enrichment results per dosage: `GO_enrichment.RDS`
- KEGG pathway enrichment results per dosage: `KEGG_enrichment.RDS`
- KEGG pathway diagrams displaying log2 fold changes in expression for genes in selected pathways: `KEGG_pathways/*.lfc.multi.png`
